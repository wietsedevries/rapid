![alt text](https://github.com/wietsedevries/rapid/blob/master/images/rapid.png "Rapid prototype CSS Framework")

Rapid CSS takes semantic HTML5 and adds markup, so you don't have to! Built on the principle of Convention over configuration it will save you a lot of time and effort.
<br/><br/>
######Check out [Wietsedevries.eu/rapid/](http://wietsedevries.eu/rapid/) for documentation.
